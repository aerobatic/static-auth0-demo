import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { /* loginUser, */ fetchQuote } from '../actions'
import Navbar from '../components/Navbar'
import Quotes from '../components/Quotes'

class App extends Component {

  render() {
    const { dispatch, quote, isAuthenticated, errorMessage, profile } = this.props
    return (
      <div>
        <Navbar
          isAuthenticated={isAuthenticated}
          errorMessage={errorMessage}
          profile={profile}
          dispatch={dispatch}
        />
        { isAuthenticated &&
          <div className='container'>
            <Quotes
              onQuoteClick={() => dispatch(fetchQuote())}
              quote={quote}
            />
          </div>
        }
        { !isAuthenticated &&
          <h3>You are not authenticated</h3>
        }
      </div>
    )
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  quote: PropTypes.string,
  isAuthenticated: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string
}

function mapStateToProps(state) {

  const { quotes, auth } = state
  const { quote } = quotes
  const { isAuthenticated, errorMessage, profile } = auth

  return {
    quote,
    isAuthenticated,
    errorMessage,
    profile
  }
}

export default connect(mapStateToProps)(App)
