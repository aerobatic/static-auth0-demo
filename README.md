* Register for Auth0
* Create new client of type "Single Page Application" in Auth0
* Turn on the GitHub social provider and follow instructions on obtaining a ClientID from https://auth0.com/docs/connections/social/github
* Make sure the GitHub connection is turned on for your client
* Important: Be sure to add localhost:3000 to the list of Allowable callback URLs
* Create a `.env` file in the root of the repo with the following contents:

~~~sh
export AUTH0_CLIENT_ID=<your auth0 client_id>
export AUTH0_CLIENT_SECRET=<your Auth0 client secret>
export AUTH0_DOMAIN=<you Auth0 client domain>

# The create-react-app setup only recognizes environment variables with the "REACT_APP_" prefix.
# Our React app only requires the CLIENT_ID and DOMAIN, not the CLIENT_SECRET
export REACT_APP_AUTHO_CLIENT_ID=$AUTH0_CLIENT_ID
export REACT_APP_AUTH0_DOMAIN=$AUTH0_DOMAIN
~~~

Make sure you have [autoenv](https://github.com/kennethreitz/autoenv) installed. Now whenever you cd into the directory the variables will be automatically set.

## Deploying to Aerobatic
To deploy to Aerobatic simply [install the Bitbucket add-on] and link the repo. When prompted if you want to deploy now, select no. On the next screen click "Environment variables" and configure values for `AUTH0_CLIENT_ID`, `AUTH0_DOMAIN` and `REACT_APP_API_URL` to the same values in your local `.env` file.

Now just push any change to your repo to trigger a build.

## Deploying the API to zeit

If you'd like to deploy your own instance of the API to zeit now, follow these steps:

* Install the zeit now CLI tool.
* Run `now` from the command line to create a new deployment. If this is the first time you've run `now`, you'll be prompted to confirm your email address.
* Once the deployment is complete your API will be available at a URL like `https://static-react-auth-demo-fwsljxnjvk.now.sh`. However be aware that everytime you deploy a new URL will be generated. In order to avoid having to repeatedly update the `REACT_APP_API_URL`, you should create an alias.

~~~sh
now alias set https://static-react-auth-demo-fwsljxnjvk.now.sh  https://quotes-api.now.sh
~~~

Now update the `REACT_APP_API_URL` environment variable in Aerobatic to the alias.

Demo based on:
https://github.com/auth0-blog/redux-auth
